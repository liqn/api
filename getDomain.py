from flask import Flask
from flask import jsonify
from flask import Flask, render_template, request
from flask.ext.mysql import MySQL
from flask import Response
import json
import sys
from pprint import pprint
from datetime import datetime, tzinfo, timedelta

app = Flask(__name__)

mysql = MySQL()

app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = 'root'
app.config['MYSQL_DATABASE_DB'] = 'api'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)

cnx = mysql.connect()

@app.errorhandler(400)
def ma_page_400(code):
	data = []
	datas = {
			'code' : code,
			'message' : "bad request",
			'datas' : data
		}	
	error = jsonify(datas)
	error.status_code = 400
	return error, code

@app.errorhandler(404)
def ma_page_404(self):
	code = 404
	datas = {
			'code' : code,
			'message' : "error"
		}	
	error = jsonify(datas)
	error.status_code = code
	return error, code

@app.errorhandler(403)
def ma_page_403(self):
	code = 403
	datas = {
			'code' : code,
			'message' : "Content-Type failed"
		}	
	error = jsonify(datas)
	error.status_code = code
	return error, code

@app.errorhandler(401)
def ma_page_401(self):
	code = 401
	datas = {
			'code' : code,
			'message' : "Authorization failed"
		}	
	error = jsonify(datas)
	error.status_code = code
	return error, code

@app.route('/api/domains.<json>', methods = ['GET'])
def getDomain(json):
	if (str(json) == "json"):
		cur = cnx.cursor()
		query = "SELECT id, slug, name, description FROM domain"
		cur.execute(query)
		columns = cur.description
		result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
		
		datas = {
			'code' : 200,
			'message' : "success",
			'datas' : result
		}
		return jsonify(datas)
	else:
		code = 400
		return ma_page_400(code)

def isExist1():
	json = "json"
	res = getDomain(json)
	if (res.status_code == 200):
		return True
	else:
		return False

@app.route('/api/domains/<mailer>.<json>', methods = ['GET'])
def getDomainMailer(mailer, json):
	if (isExist1()):
		if (str(json) == "json"):
			cur = cnx.cursor()

			queryLang = "SELECT DISTINCT `domain_lang`.`lang_id`\
						FROM `domain_lang`\
						LEFT JOIN `domain` ON `domain`.`id` = `domain_lang`.`domain_id`"
			query = "SELECT DISTINCT `domain`.`id`, `domain`.`slug`, `domain`.`name`,`domain`.`description`, `domain`.`created_at`, `user`.`id`, `user`.`username`\
						FROM `domain`\
						LEFT JOIN `user` ON  `domain`.`user_id` = `user`.`id` \
						WHERE `domain`.`name` = %s " 
			listMailer = cur.execute(query, mailer)
			if (listMailer == 0):
				code = 404
				return ma_page_404(code)
			else:
				columns = cur.description
				req = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
				cur.execute(queryLang)
				langsData = []
				for row in cur.fetchall():
					langsData.extend(row)
				return getDomain2(req, langsData)
		else:
			code = 400
			return ma_page_400(code)
	else:
		code = 404
		return ma_page_404(code)


class TZ(tzinfo):
	def utcoffset(self, dt):
		return timedelta(minutes = 60)

def transformerDate(date):
	newDate = datetime(date.year, date.month, date.day, date.hour, date.minute, date.second, tzinfo = TZ()).isoformat()
	return newDate

def getDomain2(req,langsData):
	res = {
		'code' : 200,
		'message' : "success",
		'datas' :{ 
			'langs' : langsData,
			'id': req[0].get('id'),
			'slug': req[0].get('slug'),
			'name': req[0].get('name'),
			'description': req[0].get('description'),
			'creator': {
				'id' : req[0].get('id'),
				'username' : req[0].get('username')
				},
			'created_at' : transformerDate(req[0].get('created_at'))		
		}
	}
	return jsonify(res)

def find_between(s, start, end):
  return (s.split(start))[1].split(end)[0]

def get_lang_domain(cur, domain_name):
	query_id = "SELECT `domain`.`id` FROM `domain` WHERE `domain`.`name` = %s"
	query_lang = "SELECT `domain_lang`.`lang_id` FROM `domain_lang` WHERE `domain_lang`.`domain_id` = %s"
	cur.execute(query_id, domain_name)
	id_ = cur.fetchall()
	cur.execute(query_lang, id_[0][0])
	lang = cur.fetchall()
	return lang

def getDomain3(x, cur, name):
	traduc = x.get('langs_trad')
	lang = get_lang_domain(cur, name)
	for l in lang:
		if (l[0] not in x.get('langs_trad')):
			traduc += ',' + l[0] + " : " + x.get('code')
	if (str(traduc) == "None"):
		data = {
		'trans' : []
		}
		return data
	else:
		a = traduc.split(',')
		data = {
			'trans' : {x.split(':')[0].replace(" ", ""): x.split(': ')[1] for x in a},
			'id' : x.get('translation_id'),
			'code' : x.get('code')
		}
		return data

def getDomain4(x, cur, name):
	traduc = x.get('langs_trad')
	lang = get_lang_domain(cur, name)
	data = {}
	for l in lang:
		if (l[0] not in x.get('langs_trad')):
			traduc += ',' + l[0] + " : " + x.get('code')
	if (str(traduc) == "None"):
		data = {
		'trans' : []
		}
	else:
		a = traduc.split(',')
		data = {
			'trans' : {x.split(':')[0].replace(" ", ""): x.split(': ')[1] for x in a},
			'id' : x.get('translation_id'),
			'code' : x.get('code')
		}
	return data

def VerifPass(c, r, mailer):
	query = "SELECT DISTINCT `user`.`id` \
	FROM `user` LEFT JOIN `domain` ON `user`.`id` = `domain`.`id` \
	WHERE `user`.`password` = %s and `domain`.`name` = %s"
	res = c.execute(query, [r.get('Authorization'), mailer])
	if (res == 0):
		return False
	else:
		return True

def auth_domain_user(cur, name, headers):
	query = "SELECT `user`.`password`, `domain`.`name`\
	FROM `user`\
	LEFT JOIN `domain` ON `user`.`id` = `domain`.`user_id`\
	WHERE `user`.`password` = %s  AND `domain`.`name` = %s"
	cur.execute(query, (headers.get('Authorization'), name))
	res = cur.fetchall()
	if (len(res) > 0):
		return True
	else:
		return False

def getReq(cur):
	queryGet = "SELECT `domain`.`name`, `translation_to_lang`.`translation_id`, `translation`.`code`\
	,  GROUP_CONCAT(`translation_to_lang`.`lang_id`, ' : ', `translation_to_lang`.`trans`) as langs_trad\
	FROM `domain`\
	LEFT JOIN `translation` ON `domain`.`id` = `translation`.`domain_id`\
	LEFT JOIN `translation_to_lang` ON `translation`.`id` = `translation_to_lang`.`translation_id` \
	WHERE `domain`.`name` = %s\
	GROUP BY   `translation`.`id`"
	mailer = "mailer"
	list = cur.execute(queryGet, mailer)
	if (list == 0):
		code = 404
		return ma_page_404(code)
	columns = cur.description
	result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
	req = []
	for i in result:
		req = getDomain4(i, cur, mailer)
	return req


def chek_in_trans(cur, mailer, idTrans, cle):
	query = "SELECT DISTINCT `translation_to_lang`.`lang_id`\
	FROM `translation_to_lang` \
	LEFT JOIN `translation` ON `translation`.`id` = `translation_to_lang`.`translation_id` \
	LEFT JOIN `domain` ON `domain`.`id` = `translation`.`domain_id`\
	where `domain`.`name` = %s and `translation_to_lang`.`translation_id` = %s"
	cur.execute(query, [mailer, idTrans])
	lang = cur.fetchall()
	is_exist = False
	for i in lang:
		if (cle in i):
			is_exist = True
	return is_exist	

def add_new_trans(cur, lang_id, trans, last_id):
	query = "INSERT INTO translation_to_lang  VALUES (%s, %s, %s)"
	try:
		cur.execute(query, (last_id, lang_id, trans))
		cnx.commit()
	except Exception as e:
		cnx.rollback()
		return False
	finally:
		return True

def change(req, r, cle, newvalue, cur, idDomain, lang, mailer):
	if (lang == True):
		query = "UPDATE `translation` set `code` = %s where `id` = %s"
		cur.execute(query, [newvalue, idDomain])
		cnx.commit()
	else:
		cle = cle[6:8]
		transLang = get_lang_domain(cur, mailer)
		exitCle = False
		for i in transLang:
			if (str(cle) in i):
				exitCle = True
		if (exitCle == True):
			is_exist_in_trans = chek_in_trans(cur, mailer, idDomain, cle)
			addId = idDomain
			if (is_exist_in_trans == False):
				is_add = add_new_trans(cur, cle, newvalue, addId)
			queryTrans = "UPDATE `translation_to_lang` set  `trans`= %s where `translation_id` = %s and `lang_id` = %s"		
			
			req = cur.execute(queryTrans, [newvalue, addId, cle])
			cnx.commit()
		
		else:
			return req

	return req

def translationPost(req, r, cur, mailer):
	cle = r.form
	idDomain = req['id']
	for i in cle:
		if (str(i) == "id" or str(i) == "code"):
			lang = True
		else:
			lang = False
		req = change(req, r.form, i, cle.get(i), cur, idDomain, lang, mailer)

	req = getReq(cur)
	return req


@app.route("/api/domains/<mailer>/translations.<json>", methods=['GET', 'POST'])
def translation(mailer, json):
	if (str(json) == "json" and request.method == "GET"):
		cur = cnx.cursor()
		query = "SELECT `domain`.`name`, `translation_to_lang`.`translation_id`, `translation`.`code`\
		,  GROUP_CONCAT(`translation_to_lang`.`lang_id`, ' : ', `translation_to_lang`.`trans`) as langs_trad\
		FROM `domain`\
		LEFT JOIN `translation` ON `domain`.`id` = `translation`.`domain_id`\
		LEFT JOIN `translation_to_lang` ON `translation`.`id` = `translation_to_lang`.`translation_id` \
		WHERE `domain`.`name` = %s\
		GROUP BY   `translation`.`id`"
		list = cur.execute(query, mailer)
		if (list == 0):
			code = 404
			return ma_page_404(code)
		columns = cur.description
		result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
		req = []
		for i in result:
			req.append(getDomain3(i, cur, mailer))
		res = {
			'code' : 200,
			'message' : "success",
			"datas" : req
		}
		return jsonify(res)
	elif (request.method == "POST"):
		c = "application/x-www-form-urlencoded"
		cur = cnx.cursor()
		a = VerifPass(cur, request.headers, mailer)
		if (request.headers.get('Content-Type') != c):
			code = 403
			return ma_page_403(code)
		if (request.headers.get('Authorization') == 0 or not a):
			code = 401
			return ma_page_401(code)
		if (str(json) != "json"):
			code = 400
			return ma_page_400(code)
		query = "SELECT `domain`.`name`, `translation_to_lang`.`translation_id`, `translation`.`code`\
		,  GROUP_CONCAT(`translation_to_lang`.`lang_id`, ' : ', `translation_to_lang`.`trans`) as langs_trad\
		FROM `domain`\
		LEFT JOIN `translation` ON `domain`.`id` = `translation`.`domain_id`\
		LEFT JOIN `translation_to_lang` ON `translation`.`id` = `translation_to_lang`.`translation_id` \
		WHERE `domain`.`name` = %s\
		GROUP BY   `translation`.`id`"
		list = cur.execute(query, mailer)
		if (list == 0):
			code = 404
			return ma_page_404(code)
		columns = cur.description
		result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
		req = []
		for i in result:
			req = getDomain4(i, cur, mailer)

		idTrans = req.get('id')		
		if (request.form != 0):
			para_exist = True
			for i in request.form:
				if (i != "code"):
					i = i[6:8]
					para_exist = chek_in_trans(cur, mailer, idTrans, i)
				if (para_exist == False):
					code = 403
					return ma_page_403(code)

			req = translationPost(req, request, cur, mailer)

		res = {
			'code' : 201,
			'message' : "success",
			"datas" : req
		}
		return jsonify(res),201
	else: 
		code = 400
		return ma_page_400(code)

def translation5(req, r, cur, mailer, idTrans):
	cle = r.form
	for i in cle:
		if (str(i) == "id" or str(i) == "code"):
			lang = True
		else:
			lang = False
		req = change(req, r.form, i, cle.get(i), cur, idTrans, lang, mailer)

	req = getReq5(cur, mailer, idTrans)
	return req


def getReq5(cur, mailer, idTrans):
	queryGet = "SELECT `domain`.`name`, `translation_to_lang`.`translation_id`, `translation`.`code`\
	,  GROUP_CONCAT(`translation_to_lang`.`lang_id`, ' : ', `translation_to_lang`.`trans`) as langs_trad\
	FROM `domain`\
	LEFT JOIN `translation` ON `domain`.`id` = `translation`.`domain_id`\
	LEFT JOIN `translation_to_lang` ON `translation`.`id` = `translation_to_lang`.`translation_id` \
	WHERE `domain`.`name` = %s and `translation`.`id` = %s\
	GROUP BY   `translation`.`id`"
	list = cur.execute(queryGet, [mailer, idTrans])
	if (list == 0):
		code = 404
		return ma_page_404(code)
	columns = cur.description
	result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
	req = []
	for i in result:
		req = getDomain4(i, cur, mailer)
	return req

@app.route("/api/domains/<mailer>/translations/<idTrans>.<json>", methods=['PUT', 'DELETE'])
def translationPut(mailer, idTrans, json):
	if (request.method == "PUT"):
		c = "application/x-www-form-urlencoded"
		cur = cnx.cursor()
		a = VerifPass(cur, request.headers)
		if (request.headers.get('Content-Type') != c):
			code = 403
			return ma_page_403(code)
		if (request.headers.get('Authorization') == 0 or not a):
			code = 401
			return ma_page_401(code)
		if (str(json) != "json"):
			code = 400
			return ma_page_400(code)
		query = "SELECT `domain`.`name`, `translation_to_lang`.`translation_id`, `translation`.`code`\
		,  GROUP_CONCAT(`translation_to_lang`.`lang_id`, ' : ', `translation_to_lang`.`trans`) as langs_trad\
		FROM `domain`\
		LEFT JOIN `translation` ON `domain`.`id` = `translation`.`domain_id`\
		LEFT JOIN `translation_to_lang` ON `translation`.`id` = `translation_to_lang`.`translation_id` \
		WHERE `domain`.`name` = %s and `translation`.`id` = %s\
		GROUP BY   `translation`.`id`"
		list = cur.execute(query, [mailer, idTrans])
		if (list == 0):
			code = 404
			return ma_page_404(code)
		columns = cur.description
		result = [{columns[index][0]:column for index, column in enumerate(value)} for value in cur.fetchall()]
		
		req = []
		if (request.form != 0):
			req = translation5(req, request, cur, mailer, idTrans)

		res = {
			'code' : 200,
			'message' : "success",
			"datas" : req
		}
		return jsonify(res)
		"""elif ( request.method == "DELETE"):
		cur = cnx.cursor()
		query = "DELETE FROM `translation` WHERE `translation`.`id` = %s"
		cnx.commit()
		list = cur.execute(query, idTrans)
		if (list == 0):
			code = 404
			return ma_page_404(code)
		else:
			res = {
				'code' : 200,
				'message' : "success",
				"datas" : {"id" : idTrans}
			}
"""
	else:
		code = 400
		return ma_page_400(code)


if __name__ == "__main__":
	app.config['JSON_SORT_KEYS'] = False
	app.run('0.0.0.0', port=int(sys.argv[1]), debug=True)